﻿using UnityEngine; 
using System.Collections;

public class Player : MonoBehaviour {

	GameManager gameManager;
	BoardManager boardManager;
	int lastMoveTime = 0;//koliko frame-ov je minilo od zadnjega premika
	public int moveDelay = 10;//na koliko frame-ov se lahko player premakne
	bool moving = false;
	int moveKeyDown = 0;//koliko frame-ov je že pritisnjena tipka za premik

	public GameObject cat;
	public GameObject sleepingCat;
	public GameObject cheese;

	int numberOfEnemies = 0;//koliko je trenutno premikajočih mačk
	public int cheeseScores = 100;

	Scores scores;

	// Use this for initialization
	void Start ()
	{
		boardManager = FindObjectOfType<BoardManager> ();
		gameManager = FindObjectOfType<GameManager> ();
		scores = GameObject.FindGameObjectWithTag ("Scores").GetComponent<Scores> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		lastMoveTime++;

		int horizontal = 0;
		int vertical = 0;

		horizontal = (int)Input.GetAxisRaw ("Horizontal");
		vertical = (int)Input.GetAxisRaw ("Vertical");

		if (horizontal != 0 || vertical != 0)
		{
			moveKeyDown++;
		}
		else if (horizontal == 0 && vertical == 0)
		{
			moving = false;
			moveKeyDown = 0;
		}

		numberOfEnemies = 0;
		GameObject[] catsAlive = GameObject.FindGameObjectsWithTag ("Enemy");
		{
			foreach (GameObject cat in catsAlive)
			{
				numberOfEnemies++;
			}
		}

		if (numberOfEnemies == 0)
		{
			GameObject[] sleepingCats = GameObject.FindGameObjectsWithTag ("SleepingEnemy");
			{
				foreach (GameObject sleepingCat in sleepingCats)
				{
					Coord catPos = new Coord ((int)sleepingCat.transform.position.x, (int)sleepingCat.transform.position.y);

					boardManager.board [catPos.X, catPos.Y].RemoveAt (boardManager.board [catPos.X, catPos.Y].Count-1);
					boardManager.board [catPos.X, catPos.Y].Add(Instantiate(cheese, new Vector3(catPos.X, catPos.Y, 0f), Quaternion.identity) as GameObject);
					Destroy (sleepingCat);
				}
			}

			boardManager.SpawnCats (gameManager.GetLevel);
		}

		//pogoj, kdaj se lahko premakneš
		if (boardManager.board != null && (horizontal != 0 || vertical != 0) && !moving || (boardManager.board != null && (horizontal != 0 || vertical != 0) && moving && moveKeyDown > 30 && lastMoveTime >= moveDelay))
		{
			bool canMove = CanMove ((int)transform.position.x, (int)transform.position.y, horizontal, vertical, false);

			//pogleda, če je kakšen maček ujet v polje 1x1 in ga spremeni v spečega
			GameObject[] cats = GameObject.FindGameObjectsWithTag ("Enemy");
			foreach (GameObject cat in cats)
			{
				Coord catPos = new Coord ((int)cat.transform.position.x, (int)cat.transform.position.y);
				bool trapped = true;//ali je maček ujet v polje 1x1
				for (int x = -1; x <= 1; x++)
				{
					for (int y = -1; y <= 1; y++)
					{
						if (IsInMapRange (catPos.X + x, catPos.Y + y))
						{
							if (boardManager.board [catPos.X + x, catPos.Y + y] [boardManager.board [catPos.X + x, catPos.Y + y].Count - 1].tag == "Untagged" || boardManager.board [catPos.X + x, catPos.Y + y] [boardManager.board [catPos.X + x, catPos.Y + y].Count - 1].tag == "Player")
							{
								trapped = false;
								break;
							}
						}
					}

					if (!trapped)
						break;
				}

				if (trapped)
				{
					numberOfEnemies--;
					boardManager.board [catPos.X, catPos.Y].RemoveAt (boardManager.board [catPos.X, catPos.Y].Count-1);
					if (numberOfEnemies > 0)
					{ boardManager.board [catPos.X, catPos.Y].Add (Instantiate (sleepingCat, new Vector3 (catPos.X, catPos.Y, 0f), Quaternion.identity) as GameObject); }
					else
					{ boardManager.board [catPos.X, catPos.Y].Add (Instantiate (cheese, new Vector3 (catPos.X, catPos.Y, 0f), Quaternion.identity) as GameObject); }
					Destroy (cat);

				}
			}

			//pogleda, če je kakšen speči maček, ki ni več ujet v polje 1x1 in ga spremeni nazaj v premikajočega
			GameObject[] sleepingCats = GameObject.FindGameObjectsWithTag ("SleepingEnemy");
			foreach (GameObject sleepingCat in sleepingCats)
			{
				Coord catPos = new Coord ((int)sleepingCat.transform.position.x, (int)sleepingCat.transform.position.y);
				bool trapped = true;
				for (int x = -1; x <= 1; x++)
				{
					for (int y = -1; y <= 1; y++)
					{
						if (IsInMapRange (catPos.X + x, catPos.Y + y))
						{
							if (boardManager.board [catPos.X + x, catPos.Y + y] [boardManager.board [catPos.X + x, catPos.Y + y].Count - 1].tag == "Untagged" || boardManager.board [catPos.X + x, catPos.Y + y] [boardManager.board [catPos.X + x, catPos.Y + y].Count - 1].tag == "Player")
							{
								trapped = false;

								numberOfEnemies++;
								boardManager.board [catPos.X, catPos.Y].RemoveAt (boardManager.board [catPos.X, catPos.Y].Count-1);
								boardManager.board [catPos.X, catPos.Y].Add(Instantiate(cat, new Vector3(catPos.X, catPos.Y, 0f), Quaternion.identity) as GameObject);
								Destroy (sleepingCat);
								break;
							}
						}
					}

					if (!trapped)
						break;
				}
			}

			moving = true;
			lastMoveTime = 0;
		}
		else
		{
			
		}

		if (IsPlayerCatched ())
		{
			gameManager.LostHP ();
		}
	}

	bool IsPlayerCatched()//ali je maček na istem polju kot player - ali je maček ujel playerja
	{
		Coord playerPos = new Coord ((int)transform.position.x, (int)transform.position.y);
		for (int i = 0; i < boardManager.board [playerPos.X, playerPos.Y].Count; i++)
		{
			if (boardManager.board [playerPos.X, playerPos.Y] [i].tag == "Enemy")
			{
				return true;
			}
		}

		return false;
	}

	bool IsInMapRange(int x, int y)//ali je polje x,y znotraj mape, ali je že čez rob
	{
		return x >= 0 && x < boardManager.columns && y >= 0 && y < boardManager.rows;
	}

	struct Coord//navaden Point
	{
		public int X;
		public int Y;

		public Coord(int x, int y)
		{
			X = x;
			Y = y;
		}
	}

	bool CanMove(int x, int y, int horizontal, int vertical, bool cat)//rekurzivno gleda, ali je naslednje polje možno za premik
	{
		Coord MoveTo = new Coord (x + horizontal, y + vertical);

		if (MoveTo.X < 0 || MoveTo.X >= boardManager.columns || MoveTo.Y < 0 || MoveTo.Y >= boardManager.rows)
		{ return false; }

		if (boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "Cheese")
		{
			boardManager.board [MoveTo.X, MoveTo.Y].RemoveAt (boardManager.board [MoveTo.X, MoveTo.Y].Count - 1);
			GameObject[] cheeses = GameObject.FindGameObjectsWithTag ("Cheese");
			foreach (GameObject cheese in cheeses)
			{
				if (cheese.transform.position.x == MoveTo.X && cheese.transform.position.y == MoveTo.Y)
				{
					Destroy (cheese);
					break;
				}
			}

			if (boardManager.board [x, y][boardManager.board[x, y].Count-1].tag == "Player")//če gre player na polje sira, ne pa kocka
			{ scores.AddScores (cheeseScores); }

			for (int i = 0; i < boardManager.board[MoveTo.X, MoveTo.Y].Count; i++)
			{
				Debug.Log(boardManager.board [MoveTo.X, MoveTo.Y][i].tag);
			}

			MoveObject (new Coord (x, y), MoveTo);
			return true;
		}
		else if (boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "Untagged")
		{
			MoveObject (new Coord (x, y), MoveTo);
			return true;
		}
		else if (boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "Enemy")
		{
			if (boardManager.board [x, y][boardManager.board[x, y].Count-1].tag == "Player")//če gre player na polje, kjer je enemy
			{
				MoveObject (new Coord (x, y), MoveTo);
				return true;
			}

			bool canMove = CanMove (MoveTo.X, MoveTo.Y, horizontal, vertical, true);

			if (canMove)
			{ MoveObject(new Coord (x, y), MoveTo); }
			else//poglej še ostale dve smeri
			{
				if (horizontal == 0)
				{
					canMove = CanMove (MoveTo.X, MoveTo.Y, 1, 0, true);

					if (canMove)
					{ MoveObject (new Coord (x, y), MoveTo); }
					else
					{
						canMove = CanMove (MoveTo.X, MoveTo.Y, -1, 0, true);

						if (canMove)
						{ MoveObject (new Coord (x, y), MoveTo); }
					}
					return canMove;
				}
				else if (vertical == 0)
				{
					canMove = CanMove (MoveTo.X, MoveTo.Y, 0, 1, true);

					if (canMove)
					{ MoveObject (new Coord (x, y), MoveTo); }
					else
					{
						canMove = CanMove (MoveTo.X, MoveTo.Y, 0, -1, true);

						if (canMove)
						{ MoveObject (new Coord (x, y), MoveTo); }
					}
					return canMove;
				}
			}

			return canMove;
		}
		else if (boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "Pushable")
		{
			if (cat)
				return false;
			
			bool canMove = CanMove (MoveTo.X, MoveTo.Y, horizontal, vertical, false);

			if (canMove)
			{ MoveObject (new Coord (x, y), MoveTo); }

			return canMove;
		}
		else if (boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "Blocking" || boardManager.board [MoveTo.X, MoveTo.Y][boardManager.board[MoveTo.X, MoveTo.Y].Count-1].tag == "SleepingEnemy")
		{
			return false;
		}

		return false;
	}

	void MoveObject(Coord tileFrom, Coord tileTo)//premakne objekt iz enega polja board-a na drugo polje
	{
		boardManager.board [tileTo.X, tileTo.Y].Add (boardManager.board [tileFrom.X, tileFrom.Y] [boardManager.board [tileFrom.X, tileFrom.Y].Count - 1]);//zadnji element na trenutnem polju dodaš na sosednjega
		boardManager.board [tileTo.X, tileTo.Y] [boardManager.board [tileTo.X, tileTo.Y].Count - 1].transform.position = new Vector3 (tileTo.X, tileTo.Y, 0f);//elementu, ki si ga dodal na sosednje polje iz trenutnega, popraviš pozicijo
		boardManager.board [tileFrom.X, tileFrom.Y].RemoveAt (boardManager.board [tileFrom.X, tileFrom.Y].Count - 1);//iz trenutnega polja zbrišeš element, ki je dodan na sosednje polje
	}
}

﻿using UnityEngine;
using System;//za serializable attribute - loh modify-aš, kako se elementi v hierarhiji, inspectorju vidijo ali so skriti, ...
using System.Collections.Generic;//za liste
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {
	
	GameManager gameManager;
	Scores scores;

	public int columns = 21;
	public int rows = 21;
	public int emptyZoneWidth = 3;
	public GameObject emptyTile;
	public GameObject movableWallTile;
	public GameObject fixedWallTile;
	public GameObject cheeseTile;
	public GameObject catTile;
	public GameObject mouseTile;
	public GameObject hp;

	private Transform boardHolder;//samo zato, da je hierarchy clean, game objects so pa potem children od tega
	private List<Vector3> gridPositions = new List<Vector3>();//vse pozicije znotraj mape razen sredje kocke za playerja - za fixedWalls
	private List<Vector3> emptyGridPositions = new List<Vector3>();//vse pozicije, ki so še untagged oziroma proste
	private List<Vector3> emptyBorderPositions = new List<Vector3>();//vse pozicije okoli začetnega kvadrata premičnih sten, ki so še untagged oziroma proste

	public List<GameObject>[,] board;//2D array listov GameObject-ov, prvi objekt je vedno emptyTile, drugi pa lahko stena, player, sir, ...

	int[] catsOnLevel = {9, 10};
	public int catsSpawned = 0;

	public List<GameObject> hps;

	[HideInInspector] public GameObject player;

	struct Coord//navaden Point
	{
		public int X;
		public int Y;

		public Coord(int x, int y)
		{
			X = x;
			Y = y;
		}
	}

	void Start ()
	{
		gameManager = GetComponent<GameManager> ();
		scores = GameObject.FindGameObjectWithTag ("Scores").GetComponent<Scores> ();
	}

	void InitialiseList()
	{
		UpdateEmptyGrid (true);
	}

	void BoardSetup()//postavi vsa prazna polja, fiksne in premikajoče stene
	{
		board = new List<GameObject>[columns, rows];
		boardHolder = new GameObject ("Board").transform;

		for (int x = -1; x < columns + 1; x++)
		{
			for (int y = -1; y < rows + 1; y++)
			{
				GameObject toInstantiate = emptyTile;
				if (x == -1 || x == columns || y == -1 || y == rows)
				{
					toInstantiate = fixedWallTile;
				}
				else
				{
					board [x, y] = new List<GameObject> ();
					toInstantiate = emptyTile;
				}

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
				if (x == -1 || x == columns || y == -1 || y == rows)
				{ }
				else
				{ board [x, y].Add(instance); }

				if (x == columns / 2 && y == rows / 2)//player
				{
					toInstantiate = emptyTile;
				}
				else if (x > -1 + emptyZoneWidth && x < columns - emptyZoneWidth && y > -1 + emptyZoneWidth && y < rows - emptyZoneWidth)
				{
					toInstantiate = movableWallTile;
				}
				else
				{
					continue;//potem more preskočit spodnjo kodo, ki naredi nov objekt
				}

				GameObject instance2 = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance2.transform.SetParent (boardHolder);
				board [x, y].Add(instance2);
			}
		}

		if (gameManager != null && gameManager.GetLevel == 2)
		{
			UpdateEmptyGrid (false);

			for (int i = 0; i < 19; i++)
			{
				GameObject instance = Instantiate (fixedWallTile, RandomPosition (), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
				board [(int)instance.transform.position.x, (int)instance.transform.position.y].Add (instance);

				//če je dodani zid v istem stolpcu ali vrstici kot player, iz gridPositions izbrišemo vse ostale pozicije iz istega stolpca ali vrstice, ker se drugače lahko zgodi, da so levo, desno, gor in dol od playerja zidovi in se sploh ne more premaknit
				if ((int)instance.transform.position.x == columns / 2)
				{
					for (int j = 0; j < gridPositions.Count; j++)
					{
						if (gridPositions[j].x == columns / 2)
						{
							gridPositions.RemoveAt(j);
							j--;
						}
					}
				}
				else if ((int)instance.transform.position.y == rows / 2)
				{
					for (int j = 0; j < gridPositions.Count; j++)
					{
						if (gridPositions[j].y == rows / 2)
						{
							gridPositions.RemoveAt(j);
							j--;
						}
					}
				}
			}
		}
	}

	public void InitHP(int HP)//naredi miši nad mapo, ki nakazujejo preostala življenja
	{
		hps = new List<GameObject> ();
		for (int i = 0; i < HP; i++)
		{
			hps.Add (Instantiate (hp, new Vector3(i, rows + 1.25f, 0f), Quaternion.identity) as GameObject);
		}
	}

	float PointToPointDistance(float tile1X, float tile1Y, float tile2X, float tile2Y)
	{
		return Mathf.Sqrt((tile1X - tile2X) * (tile1X - tile2X) + (tile1Y - tile2Y) * (tile1Y - tile2Y));
	}

	Vector3 RandomPosition()//vrne random pozicije znotraj celotne igralne površine
	{
		int randomIndex = Random.Range(0, gridPositions.Count);
		Vector3 randomPosition = gridPositions[randomIndex];
		gridPositions.RemoveAt (randomIndex);
		return randomPosition;
	}

	Vector3 RandomEmptyGridPosition()//vrne random pozicije znotraj celotne igralne površine, ki je še prosta
	{
		int randomIndex = Random.Range(0, emptyGridPositions.Count);
		Vector3 randomPosition = emptyGridPositions[randomIndex];
		emptyGridPositions.RemoveAt (randomIndex);
		return randomPosition;
	}

	Vector3 RandomEmptyBorderPosition()//vrne random pozicije znotraj polj, ki so na začetku levela empty
	{
		int randomIndex = Random.Range(0, emptyBorderPositions.Count);
		Vector3 randomPosition = emptyBorderPositions[randomIndex];
		emptyBorderPositions.RemoveAt (randomIndex);
		return randomPosition;
	}

	Vector3 FarRandomEmptyBorderPosition()//random pozicija, ki ni blizu playerja in je znotraj polj, ki so na začetku levela empty
	{
		Vector3 randomPosition = RandomEmptyBorderPosition ();
		while (PointToPointDistance(randomPosition.x, randomPosition.y, player.transform.position.x, player.transform.position.y) < (columns + rows) / 4)
		{ randomPosition = RandomEmptyBorderPosition (); }
		return randomPosition;
	}

	void LayoutObjectAtRandom(GameObject tile, int minimum, int maximum, bool onEmptyTileOnly)//naredi naključno število med minimum in maximum objektov
	{
		int objectCount = Random.Range (minimum, maximum + 1);
		for (int i = 0; i < objectCount; i++)
		{
			Vector3 randomPosition = RandomPosition ();
			if (onEmptyTileOnly)
			{
				randomPosition = FarRandomEmptyBorderPosition ();
			}

			board[(int)randomPosition.x, (int)randomPosition.y].Add(Instantiate(tile, randomPosition, Quaternion.identity) as GameObject);
		}
	}

	public void UpdateEmptyGrid(bool init)//če ni init, je pa le update
	{
		gridPositions.Clear ();
		emptyGridPositions.Clear ();
		emptyBorderPositions.Clear ();
		for (int x = 1; x < columns - 1; x++)
		{
			for (int y = 1; y < rows - 1; y++)
			{
				if (!(x == columns / 2 && y == rows / 2))
				{
					gridPositions.Add (new Vector3 (x, y, 0f));
					if (init || board [x, y] [board [x, y].Count - 1].tag == "Untagged")
					{ emptyGridPositions.Add (new Vector3 (x, y, 0f)); }

					if (x > -1 + emptyZoneWidth && x < columns - emptyZoneWidth && y > -1 + emptyZoneWidth && y < rows - emptyZoneWidth)
					{
					
					}
					else
					{
						if (init || board [x, y] [board [x, y].Count - 1].tag == "Untagged")
						{
							emptyBorderPositions.Add (new Vector3 (x, y, 0f));
						}
					}
				}
			}
		}
	}

	public void SpawnNewMouse()//playerja prestavi stran od mačk in ga spet aktivira
	{
		UpdateEmptyGrid (false);

		GameObject[] cats = GameObject.FindGameObjectsWithTag ("Enemy");
		bool tooClose = true;
		Coord playerPos = new Coord ((int)player.transform.position.x, (int)player.transform.position.y);

		Vector3 randomPosition = RandomPosition ();
		while(tooClose)
		{
			tooClose = false;
			randomPosition = RandomEmptyGridPosition ();
			foreach (GameObject cat in cats)
			{
				Coord catPos = new Coord ((int)cat.transform.position.x, (int)cat.transform.position.y);
				if (PointToPointDistance(randomPosition.x, randomPosition.y, catPos.X, catPos.Y) < (columns + rows) / 4)
				{
					tooClose = true;
					break;
				}
			}
		}

		player.transform.position = randomPosition;
		player.SetActive (true);
		Coord playerNewPos = new Coord ((int)player.transform.position.x, (int)player.transform.position.y);

		int playerIndex = 0;

		for (int i = 0; i < board [playerPos.X, playerPos.Y].Count; i++)
		{
			if (board [playerPos.X, playerPos.Y] [i].tag == "Player")
			{
				playerIndex = i;
				break; 
			}
		}

		board [playerNewPos.X, playerNewPos.Y].Add (player);//playerja dodaš na drugo polje
		board [playerPos.X, playerPos.Y].RemoveAt (playerIndex);//iz trenutnega polja zbrišeš element, ki je dodan na sosednje polje
	}

	public void SetupScene(int level)
	{
		BoardSetup ();
		InitialiseList ();

		player = Instantiate (mouseTile, new Vector3 (columns / 2, rows / 2, 0f), Quaternion.identity) as GameObject;//spawn player
		board [columns / 2, rows / 2].Add (player);
	}

	[HideInInspector] public int spawnNumber = 1;

	public void SpawnCats(int level)
	{
		UpdateEmptyGrid (false);

		if (level == 1 || level == 2)
		{
			if (catsSpawned < catsOnLevel [level - 1])
			{
				if (catsSpawned + spawnNumber > catsOnLevel [level - 1])
				{
					spawnNumber = catsOnLevel [level - 1] - catsSpawned;
				}

				LayoutObjectAtRandom (catTile, spawnNumber, spawnNumber, true);
				catsSpawned += spawnNumber;
				if (spawnNumber == 1)
				{ spawnNumber = 2; }
				else
				{ spawnNumber = 1; }
			}
			else
			{
				Debug.Log ("cats: " + catsSpawned + ", level: " + level);
				//level več
				gameManager.NextLevel();
			}
		}
	}
}
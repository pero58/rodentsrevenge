<<<<<<< HEAD
﻿using UnityEngine; 
using System.Collections;
using System.Collections.Generic;//rabiš za liste

=======
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
>>>>>>> 8b15e1512f0fd73343de8b42cc9078f97ad2d847
public class Enemy : MonoBehaviour {

	public BoardManager boardManager;
	private GameObject player;
	public int moveDelay = 60;//na koliko frame-ov se lahko enemy premakne
	int lastMove = 0;

	// Use this for initialization
	void Start ()
	{
		boardManager = FindObjectOfType<BoardManager> ();
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update ()
	{
		lastMove++;
		if (lastMove >= moveDelay)
		{
			lastMove = 0;
			Move ();
		}
	}

<<<<<<< HEAD
	struct Coord//navaden Point
=======
	struct Coord
>>>>>>> 8b15e1512f0fd73343de8b42cc9078f97ad2d847
	{
		public int X;
		public int Y;

		public Coord(int x, int y)
		{
			X = x;
			Y = y;
		}
	}

<<<<<<< HEAD
	void Move()//premik mačke
	{
		Coord playerTile = new Coord ((int)player.transform.position.x, (int)player.transform.position.y);
		List<Coord> tiles = new List<Coord>();
		int[,] mapFlags = new int[boardManager.columns, boardManager.rows];//tu notri so označene razdalje od mačke do dane točke
=======
	void Move()
	{
		Coord playerTile = new Coord ((int)player.transform.position.x, (int)player.transform.position.y);
		List<Coord> tiles = new List<Coord>();
		int[,] mapFlags = new int[boardManager.columns, boardManager.rows];
>>>>>>> 8b15e1512f0fd73343de8b42cc9078f97ad2d847

		Coord start = new Coord ((int)transform.position.x, (int)transform.position.y);//začetna pozicija mačke

		Queue<Coord> queue = new Queue<Coord>();
		queue.Enqueue(new Coord (start.X, start.Y));
		mapFlags[start.X, start.Y] = 1;

		while (queue.Count > 0)//iskanje poti po A-star metodi
		{
			Coord tile = queue.Dequeue();
			tiles.Add(tile);

			if (boardManager.board [tile.X, tile.Y] [boardManager.board [tile.X, tile.Y].Count - 1].tag == "Player")//najdi pot nazaj
			{
<<<<<<< HEAD
				while (mapFlags [tile.X, tile.Y] >= 1)//ta pogoj je samo za varnost, lahko bi blo tudi while(true)
				{
					if (mapFlags [tile.X, tile.Y] == 2)//2 je sosednji tile, zato se more premaknit nanj
=======
				while (mapFlags [tile.X, tile.Y] >= 1)
				{
					if (mapFlags [tile.X, tile.Y] == 2)
>>>>>>> 8b15e1512f0fd73343de8b42cc9078f97ad2d847
					{
						MoveObject (start, tile);
						return;
					}

					bool breaking = false;
					for (int x = tile.X - 1; x <= tile.X + 1; x++)
					{
						for (int y = tile.Y - 1; y <= tile.Y + 1; y++)
						{
							if (IsInMapRange (x, y))
							{
								if (mapFlags [x, y] == mapFlags [tile.X, tile.Y] - 1)
								{
									tile.X = x;
									tile.Y = y;

									breaking = true;
									break;
								}
							}
						}

						if (breaking)
						{ break; }
					}
				}
			}

			for (int x = tile.X - 1; x <= tile.X + 1; x++)
			{
				for (int y = tile.Y - 1; y <= tile.Y + 1; y++)
				{
					//if (IsInMapRange(x, y) && (y == tile.Y || x == tile.X))//gleda samo 4 okoli
					if (IsInMapRange(x, y))//gleda 8 polj okoli
					{
						if(mapFlags[x,y] == 0 && (boardManager.board[x,y][boardManager.board[x,y].Count-1].tag == "Untagged" || boardManager.board[x,y][boardManager.board[x,y].Count-1].tag == "Player"))//mačka lahko gre na prazno polje ali polje, kjer je player
						{
							mapFlags[x,y] = mapFlags[tile.X, tile.Y] + 1;
							queue.Enqueue(new Coord(x, y));
						}
					}
				}
			}
		}

		//ne more priti do playerja, zato pogleda samo 8 polj okoli playerja in se odloči za najbližjega playerju
		Coord bestTile = new Coord();
		int bestTileDistance = -1;
		for (int x = start.X - 1; x <= start.X + 1; x++)
		{
			for (int y = start.Y - 1; y <= start.Y + 1; y++)
			{
				if (IsInMapRange (x, y))
				{
					if ((x != start.X || y != start.Y) && boardManager.board[x,y][boardManager.board[x,y].Count-1].tag == "Untagged")
					{
						Coord currentTile = new Coord (x, y);
						int currentTileDistance = PointToPointDistance (currentTile, playerTile);
						if (bestTileDistance == -1 || currentTileDistance < bestTileDistance)
						{
							bestTile = currentTile;
							bestTileDistance = currentTileDistance;
						}
					}
				}
			}
		}

		if (bestTileDistance != -1)
		{
			MoveObject (start, bestTile);
		}
	}

	void MoveObject(Coord tileFrom, Coord tileTo)//premakne objekt iz enega polja board-a na drugo polje
	{
		boardManager.board [tileTo.X, tileTo.Y].Add (boardManager.board [tileFrom.X, tileFrom.Y] [boardManager.board [tileFrom.X, tileFrom.Y].Count - 1]);
		boardManager.board [tileTo.X, tileTo.Y] [boardManager.board [tileTo.X, tileTo.Y].Count - 1].transform.position = new Vector3 (tileTo.X, tileTo.Y, 0f);
		boardManager.board [tileFrom.X, tileFrom.Y].RemoveAt (boardManager.board [tileFrom.X, tileFrom.Y].Count - 1);
	}

	int PointToPointDistance(Coord tile1, Coord tile2)//diagonala po pitagori, ker ni potrebna dejanska razdalja, ampak le primerjava, sem koren vsote spustil zaradi hitrejšega računanja
	{
		return (tile1.X - tile2.X) * (tile1.X - tile2.X) + (tile1.Y - tile2.Y) * (tile1.Y - tile2.Y);
	}

	bool IsInMapRange(int x, int y)//ali je polje x,y znotraj mape, ali je že čez rob
	{
		return x >= 0 && x < boardManager.columns && y >= 0 && y < boardManager.rows;
	}
}

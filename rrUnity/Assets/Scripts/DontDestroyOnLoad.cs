﻿using UnityEngine;
using System.Collections;

public class DontDestroyOnLoad : MonoBehaviour {

	public static DontDestroyOnLoad instance = null;

	//celotna naloga te skripte je ta, da se ob loadanju novega levela točke ne zbrišejo skupaj z ostalimi objekti, ampak ostane skripta taka kot je

	void Awake ()
	{
		if (instance == null)
		{ instance = this; }
		else if (instance != this)
		{ Destroy (gameObject); }

		DontDestroyOnLoad (gameObject);//da ga ne uniči, ko sceno zloudaš - ker hočemo točke prenesti v naslednji level...
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
 
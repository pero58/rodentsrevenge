﻿using UnityEngine;
using System; 
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;//ta skripta je zdej singleton, kar pomeni da ne more obstajati naenkrat več kot ena instanca tega objekta, hkrati pa nardi metode v razredu static
	public BoardManager boardScript;

	public int level = 1;
	public int scoresPerLevelMultiplier = 100;
	Scores scores;
	GameObject gameOver;

	//public GameObject hp;
	public int startHP = 3;
	int HP = 3;

	public int GetLevel
	{
		get { return this.level; }
	}

	// Use this for initialization
	void Awake ()
	{
		if (instance == null)
		{ instance = this; }
		else if (instance != this)
		{ Destroy (gameObject); }

		HP = startHP;
		DontDestroyOnLoad (gameObject);//da ga ne uniči, ko sceno zloudaš - ker hočemo točke prenesti v naslednji level...
		boardScript = GetComponent<BoardManager> ();
		InitGame ();
		scores = GameObject.FindGameObjectWithTag ("Scores").GetComponent<Scores> ();
		gameOver = GameObject.Find("GameOver");
		gameOver.SetActive (false);
	}

	void InitGame()
	{
		boardScript.SetupScene (level);
		//Debug.Log ("init");
		boardScript.InitHP(HP);
	}

	private void OnLevelWasLoaded(int index)
	{
		boardScript.catsSpawned = 0;
		boardScript.spawnNumber = 1;
		InitGame ();
	}

	public void NextLevel()
	{
		scores.AddScores(level * scoresPerLevelMultiplier);
		level++;
		//Debug.Log ("nextLevel");
		Application.LoadLevel(Application.loadedLevel);
		//boardScript = GetComponent<BoardManager> ();
		//InitGame ();
	}

	public void LostHP()
	{
		HP--;
		GameObject lastHP = boardScript.hps [boardScript.hps.Count - 1];
		boardScript.hps.RemoveAt (boardScript.hps.Count - 1);
		Destroy (lastHP);
		boardScript.player.SetActive (false);
		if (HP == 0)
		{
			GameOver ();
		}
		else
		{
			//spawn new mouse
			boardScript.SpawnNewMouse();
		}
	}

	void GameOver()
	{
		gameOver.SetActive (true);
		Debug.Log ("setGameOver");
		HP = startHP;
		Invoke("ResetGame", 2);
	}

	void ResetGame()
	{
		gameOver.SetActive (false);
		scores.Reset ();
		level = 1;
		Application.LoadLevel(Application.loadedLevel);
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.Escape))
		{
			Application.Quit ();
		}
	}
}